FROM ubuntu:latest
RUN apt update
RUN apt-get -y install nginx
RUN apt-get -y install nodejs 
RUN apt-get -y install npm
RUN apt install -y mariadb-server
RUN service nginx start
RUN systemctl start mysql.service
RUN mkdir -p /var/www/code/todolist
WORKDIR /var/www/code/todolist
COPY code/todolist ./
WORKDIR /var/www/code/todolist/frontend
RUN npm install
RUN npm run build
RUN cp -R build/* /var/www/html
WORKDIR /var/www/code/todolist
RUN npm install
EXPOSE 8080
CMD ["npm", "start"]
