apt -y update

sudo apt remove -y nodejs
sudo apt-get -y autoremove
sudo apt install -y curl
curl -sL https://deb.nodesource.com/setup_16.x | sudo -E bash -
sudo apt install -y nodejs

sudo apt install -y mariadb-server
sudo systemctl start mysql.service

sudo npm install pm2 -g

sudo cp -R /vagrant/code/todolist /var/local/
sudo -i
cd /var/local/todolist
npm install

mysql -u root -e 'CREATE DATABASE todolist;'
mysql -u root -e 'GRANT ALL PRIVILEGES ON todolist.*  TO 'todo_user'@'localhost' IDENTIFIED BY "abc123";'
mysql -u todo_user -pabc123 todolist < /var/local/todolist/database_schema/dump.sql
pm2 start 'npm start'